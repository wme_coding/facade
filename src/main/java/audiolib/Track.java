package audiolib;

public class Track {
    private String name;
    private Formats format;

    public Track(String name, Formats format) {
        this.name = name;
        this.format = format;
    }

    public String getFullName(){
        return name + "." + format.getFormat();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Formats getFormat() {
        return format;
    }

    public void setFormat(Formats format) {
        this.format = format;
    }
}
