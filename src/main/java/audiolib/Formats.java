package audiolib;

public enum Formats {
    MP3("mp3"), WAV("wav"), APE("ape");

    private String format;

    Formats(String format) {
        this.format = format;
    }

    public String getFormat(){
        return format;
    }
}
