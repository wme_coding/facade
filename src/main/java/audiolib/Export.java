package audiolib;

public class Export {

    public Track atMp3(Track track){
        track.setFormat(Formats.MP3);
        return track;
    }

    public Track atWav(Track track){
        track.setFormat(Formats.WAV);
        return track;
    }

    public Track atApe(Track track){
        track.setFormat(Formats.APE);
        return track;
    }


}
