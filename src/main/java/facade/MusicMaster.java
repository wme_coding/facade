package facade;

import audiolib.*;

public class MusicMaster {

    public Track master(Track track, Formats finalFormat){
        Compressor compressor = new Compressor();
        Limiter limiter = new Limiter();
        Normalizer normalizer = new Normalizer();
        NoiseReduction noiseReduction = new NoiseReduction();
        Export export = new Export();

        noiseReduction.noiseReduction(track);
        normalizer.normalizeTrack(track);
        compressor.compressTrack(track);
        limiter.limit(track);
        normalizer.normalizeTrack(track);
        return export.atMp3(track);
    }
}
