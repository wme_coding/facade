import audiolib.Formats;
import audiolib.Track;
import facade.MusicMaster;
import org.junit.Assert;
import org.junit.Test;

public class TestMastering {

    @Test
    public void testMastering(){
        Track recording = new Track("untitled", Formats.WAV);
        MusicMaster musicMaster = new MusicMaster();

        musicMaster.master(recording, Formats.MP3);
        Assert.assertEquals("untitled.mp3", recording.getFullName());
    }
}
